@extends('layouts.app')

@section('content')
@if(count($posts) > 0)
@foreach($posts as $post)
@if(Auth::user())
@if($post->isActive === 1)
<div class="card text-center">
  <div class="card-body">


    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
    <h6 class="card-title mb-3">Author: {{$post->user->name}}</h6>
    <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>

  </div>
  @endif
  @endif

  @if(Auth::user())
  @if(Auth::user()->id == $post->user_id)
  @if($post->isActive === 1)
  <div class="card-footer">

    <form method="POST" action="/posts/{{$post->id}}">
      @method('DELETE')
      @csrf
      <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">
        Edit Post
      </a>

      <button type="submit" class="btn btn-danger">Delete Post</button>
    </form>
  </div>
  @endif
  @endif
  @endif
</div>
@endforeach
@else
<div>
  <h2>There are no posts to display</h2>
  <a href="/posts/create" class="btn btn-info">Create Post</a>
</div>
@endif
@endsection
